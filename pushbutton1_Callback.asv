% Sets three markers
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)    
    global frameRate;
    

    handles.w.HardHat.rotation = [0 0 1 0];
   
    lastFrameTime = -1.0;
    lastFrameID = -1.0;
    usePollingLoop = false;         % approach 1 : poll for mocap data in a tight loop using GetLastFrameOfData
    usePollingTimer = false;        % approach 2 : poll using a Matlab timer callback ( better for UI based apps )
    useFrameReadyEvent = true;      % approach 3 : use event callback from NatNet (no polling)
    useUI = true;
    
    
    handles.counter = 1;
    handles.label_comps = {'pt2_lbl', 'pt4_lbl', 'pt1_lbl'};
    handles.chkbx_comps = {'pt2_chkbx', 'pt4_chkbx', 'pt1_chkbx'};
    handles.disNames = {'pt2_distances', 'pt4_distances', 'pt1_distances'};
    handles.pts = {'p2', 'p4', 'p1'};
    handles.isdonesetting=false;
    handles.pt2_distances = [0 0 0 0];
    handles.pt4_distances = [0 0 0 0];
    handles.pt1_distances = [0 0 0 0];
    handles.isfdown = false;
    
    handles.initial.pts = zeros(3,3);
    handles.initial.cs.i = [0 0 0];
    handles.initial.cs.j = [0 0 0];
    handles.initial.cs.k = [0 0 0];
    
    handles.w.(handles.pts{handles.counter}).children.appearance.material.diffuseColor = [1 0 0];
    guidata(handles.figure1, handles);
    
    %uncheck the boxes
    for i = 1:3
        set(handles.(handles.chkbx_comps{i}), 'Value', 0);
    end
    persistent arr;
    % Open figure
    if(useUI)
        hFigure = msgbox(sprintf('Make sure the subject is still.\nIdentify landmark locations with marker base.\nPress ''F'' to confirm locations.\nPress OK when finished.'), 'Set Marker Points');
        set(hFigure, 'WindowKeyPressFcn', @(src, event)keydown(src, event, hObject));
    end

    try
        % Add NatNet .NET assembly so that Matlab can access its methods, delegates, etc.
        % Note : The NatNetML.DLL assembly depends on NatNet.dll, so make sure they
        % are both in the same folder and/or path if you move them.
%         display('[NatNet] Creating Client.')
        % TODO : update the path to your NatNetML.DLL file here
%         dllPath = fullfile('c:','NatNetSDK2.5','lib','x64','NatNetML.dll');
        assemblyInfo = NET.addAssembly('C:\Users\admin\Documents\MATLAB\Libraries\NatNet\lib\x64\NatNetML.dll');

        % Create an instance of a NatNet client
        theClient = NatNetML.NatNetClientML(0); % Input = iConnectionType: 0 = Multicast, 1 = Unicast
        version = theClient.NatNetVersion();
        fprintf( '[NatNet] Client Version : %d.%d.%d.%d\n', version(1), version(2), version(3), version(4) );

        % Connect to an OptiTrack server (e.g. Motive)
        display('[NatNet] Connecting to OptiTrack Server.')
        hst = java.net.InetAddress.getLocalHost;
        flg = theClient.Initialize('128.235.117.39', '128.235.117.39'); % Flg = returnCode: 0 = Success
        if (flg == 0)
            display('[NatNet] Initialization Succeeded')
        else
            display('[NatNet] Initialization Failed')
        end
        
        % print out a list of the active tracking Models in Motive
%         GetDataDescriptions(theClient)
        
        % Test - send command/request to Motive
        [byteArray, retCode] = theClient.SendMessageAndWait('FrameRate');
        if(retCode ==0)
            byteArray = uint8(byteArray);
            frameRate = typecast(byteArray,'single');
        end
        
        % get the mocap data
        if(usePollingTimer)
            % approach 2 : poll using a Matlab timer callback ( better for UI based apps )
            framePerSecond = 200;   % timer frequency
            TimerData = timer('TimerFcn', {@TimerCallback,theClient},'Period',1/framePerSecond,'ExecutionMode','fixedRate','BusyMode','drop');
            start(TimerData);
            % wait until figure is closed
            uiwait(hFigure);
        else
            if(usePollingLoop)
                % approach 1 : get data by polling - just grab 5 secs worth of data in a tight loop
                for idx = 1 : 1000   
                   % Note: sleep() accepts [mSecs] duration, but does not process any events.
                   % pause() processes events, but resolution on windows can be at worst 15 msecs
                   java.lang.Thread.sleep(5);  

                    % Poll for latest frame instead of using event callback
                    data = theClient.GetLastFrameOfData();
                    frameTime = data.fLatency;
                    frameID = data.iFrame;
                    if(frameTime ~= lastFrameTime)
                        fprintf('FrameTime: %0.3f\tFrameID: %5d\n',frameTime, frameID);
                        lastFrameTime = frameTime;
                        lastFrameID = frameID;
                    else
                        display('Duplicate frame');
                    end
                 end
            else
                % approach 3 : get data by event handler (no polling)
                % Add NatNet FrameReady event handler
                ls = addlistener(theClient,'OnFrameReady2',@(src,event)FrameReadyCallback(src,event,hObject, hFigure));
                display('[NatNet] FrameReady Listener added.');
                % wait until figure is closed
                uiwait(hFigure);
            end
        end

    catch err
        display(err);
    end

    % cleanup
    if(usePollingTimer)
        stop(TimerData);
        delete(TimerData);
    end
    theClient.Uninitialize();
    if(useFrameReadyEvent)
        if(~isempty(ls))
            delete(ls);
        end
    end
    clear functions;

    display('NatNet Sample End')
    
end
function keydown(src,event, hObject)
    
    if (event.Key =='f' | event.Key =='F')
        handles = guidata(hObject);
        handles.isfdown = true;
        guidata(handles.figure1, handles);
    end
    
end
% Test : process data in a Matlab Timer callback
function TimerCallback(obj, event, theClient)

    frameOfData = theClient.GetLastFrameOfData();
    UpdateUI( frameOfData );
    
end

% Test : Process data in a NatNet FrameReady Event listener callback
function FrameReadyCallback(src, event, hObject, handles)
    
    frameOfData = event.data;
    UpdateUI( frameOfData, hObject, handles );
    
end

% Update a Matlab Plot with values from a single frame of mocap data
function UpdateUI( frameOfData, hObject, hFigure )

    persistent lastFrameTime;
    persistent lastFrameID;
    persistent hX;
    persistent hY;
    persistent hZ;
    persistent arrayIndex;
    persistent frameVals;
    persistent xVals;
    persistent yVals;
    persistent zVals;
    persistent bufferModulo;

    global frameRate;
    
    % first time - generate an array and a plot
    if isempty(hX)
        % initialize static
    end

    % calculate the frame increment based on mocap frame's timestamp
    % in general this should be monotonically increasing according
    % To the mocap framerate, however frames are not guaranteed delivery
    % so to be accurate we test and report frame drop or duplication
    newFrame = true;
    droppedFrames = false;
    frameTime = frameOfData.fLatency;
    frameID = frameOfData.iFrame;
    calcFrameInc = round( (frameTime - lastFrameTime) * frameRate );
    % clamp it to a circular buffer of 255 frames
    arrayIndex = mod(arrayIndex + calcFrameInc, bufferModulo);
    if(arrayIndex==0)
        arrayIndex = 1;
    end
    if(calcFrameInc > 1)
        % debug
        % fprintf('\nDropped Frame(s) : %d\n\tLastTime : %.3f\n\tThisTime : %.3f\n', calcFrameInc-1, lastFrameTime, frameTime);
        droppedFrames = true;
    elseif(calcFrameInc == 0)
        % debug
        % display('Duplicate Frame')      
        newFrame = false;
    end
    
    % debug
    % fprintf('FrameTime: %0.3f\tFrameID: %d\n',frameTime, frameID);
    
    try
        if(newFrame)
            if(frameOfData.RigidBodies.Length() > 0)

                rigidBody1 = frameOfData.RigidBodies(1);
                rigidBody2 = frameOfData.RigidBodies(2);
                matmarker = zeros(4,3);
                for i = 1:4
                   matmarker(i, :) = [rigidBody1.Markers(i).x, rigidBody1.Markers(i).y, rigidBody1.Markers(i).z];
                end
                pt = identifyMarkers(matmarker);
                
                if(pt~=-1)
                    handles = guidata(hObject);
                    if(~handles.isfdown)
                        
                        set(handles.(handles.label_comps{handles.counter}), 'String', sprintf('[%0.3f,%0.3f,%0.3f]', matmarker(pt,1), matmarker(pt,2), matmarker(pt,3)));
                        guidata(handles.figure1, handles);
                    else
                        handles = guidata(hObject);
                        dp = [matmarker(pt,1), matmarker(pt,2), matmarker(pt,3)];
                        handles.w.(handles.pts{handles.counter}).children.appearance.material.diffuseColor = [0.0381    0.8000    0.2285];
                        if(~handles.isdonesetting)
                            set(handles.(handles.label_comps{handles.counter}), 'String', sprintf('[%0.3f,%0.3f,%0.3f]', matmarker(pt,1), matmarker(pt,2), matmarker(pt,3)));
                            set(handles.(handles.chkbx_comps{handles.counter}), 'Value', 1);
                            
                            dis = zeros(1,4);
                            
                            for i = 1:4
                                cm = [rigidBody2.Markers(i).x, rigidBody2.Markers(i).y, rigidBody2.Markers(i).z];
                                dis(i) = norm(dp-cm);
                            end
                            
                            %store the distances to the 4 reference markers
                            handles.(handles.disNames{handles.counter}) = dis;
                            
                            %store the selected point
                            handles.initial.pts(handles.counter,:) = dp;
                            
                            if(handles.counter<3) 
                                disp('added');
                                handles.counter = handles.counter + 1;
                                handles.w.(handles.pts{handles.counter}).children.appearance.material.diffuseColor = [1 0 0];
                                handles.isfdown = false;
                            else
                                handles.isdonesetting=true;
                                
                                %create initial coordinate system, use as
                                %the reference 'zero' rotation.
                                
                                v21 = handles.initial.pts(3,:) - handles.initial.pts(1,:);
                                m21 = mean([handles.initial.pts(1,:); handles.initial.pts(2,:)]);
                                
                                v4m = handles.initial.pts(2,:) - m21;
                                
                                handles.initial.cs.i = (v21)/norm(v21); 
                                otherVecInPlane = (v4m)/norm(v4m); 
                                
                                crossprod = cross(handles.initial.cs.i, otherVecInPlane);
                                handles.initial.cs.k =  crossprod/norm(crossprod);
                                
                                handles.initial.cs.j = cross(handles.initial.cs.k ,handles.initial.cs.i)/ norm(cross(handles.initial.cs.k ,handles.initial.cs.i));
                                
                                %                 local reference frame
                                p2loc = handles.w.p2.translation;
                                p4loc = handles.w.p4.translation;
                                p1loc = handles.w.p1.translation;
                                
                                
                                computeRandL( , handles.initial.pts');
                                
                                uiresume(hFigure);
                                close(hFigure);
                            end
                        end
                        
                        guidata(handles.figure1, handles);
                        
                    end
                    
                end
           end
        end
    catch err
        display(err);
    end
    
    lastFrameTime = frameTime;
    lastFrameID = frameID;

end
