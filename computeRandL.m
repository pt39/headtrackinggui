%computes the rotation and translation from points A to point B using least
%squares pose estimation
%A- Global Coordinatate Matrix (optitrack): 3 x markers
%B- Local Coordinate Matrix (mesh model): 3 x markers;
function [R, L] = computeRandL (B, A) 
    centroidA = repmat(mean(A,2), 1, 3);
    centroidB = repmat(mean(B,2), 1, 3);

    M = (B-centroidB)*(A-centroidA)';

    [ U , ~ , V] = svd(M);

    R = U*V';
    L = centroidB(:,1) - R*centroidA(:,1);
end