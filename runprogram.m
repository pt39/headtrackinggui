% Entry point to the program
addpath(genpath('C:\Users\admin\Documents\MATLAB\Libraries\geom3d'));

w = vrworld('HardHat_markers.WRL');
open(w);


w.p1.children.appearance.material.diffuseColor = [0.0381    0.8000    0.2285];
w.p2.children.appearance.material.diffuseColor = [0.0381    0.8000    0.2285];
w.p3.children.appearance.material.diffuseColor = [0.0381    0.8000    0.2285];
w.p4.children.appearance.material.diffuseColor = [0.0381    0.8000    0.2285];
w.origin.children.appearance.material.diffuseColor = [1 1 1];
w.HardHat.rotation = [0 0 1 0];
h = main(w);
c1 = vr.canvas(w, h, [30 40 610 490]);
c1.CameraPosition = [0 0 .5];
% vrsetpref('DefaultViewer', 'internalv5')

%%

