function statichardhatdemo_btn_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)    
    global frameRate;
%     
%     figure(99);
    
    lastFrameTime = -1.0;
    lastFrameID = -1.0;
    usePollingLoop = false;         % approach 1 : poll for mocap data in a tight loop using GetLastFrameOfData
    usePollingTimer = false;        % approach 2 : poll using a Matlab timer callback ( better for UI based apps )
    useFrameReadyEvent = true;      % approach 3 : use event callback from NatNet (no polling)
    useUI = true;
    
    persistent arr;
    % Open figure
    if(useUI)
        hFigure = msgbox(sprintf('Close this pop when tracking completed.'), 'Track Head');
    end

    try
        % Add NatNet .NET assembly so that Matlab can access its methods, delegates, etc.
        % Note : The NatNetML.DLL assembly depends on NatNet.dll, so make sure they
        % are both in the same folder and/or path if you move them.
%         display('[NatNet] Creating Client.')
        % TODO : update the path to your NatNetML.DLL file here
%         dllPath = fullfile('c:','NatNetSDK2.5','lib','x64','NatNetML.dll');
        assemblyInfo = NET.addAssembly('C:\Users\admin\Documents\MATLAB\Libraries\NatNet\lib\x64\NatNetML.dll');

        % Create an instance of a NatNet client
        theClient = NatNetML.NatNetClientML(0); % Input = iConnectionType: 0 = Multicast, 1 = Unicast
        version = theClient.NatNetVersion();
        fprintf( '[NatNet] Client Version : %d.%d.%d.%d\n', version(1), version(2), version(3), version(4) );

        % Connect to an OptiTrack server (e.g. Motive)
        display('[NatNet] Connecting to OptiTrack Server.')
        hst = java.net.InetAddress.getLocalHost;
        flg = theClient.Initialize('128.235.117.39', '128.235.117.39'); % Flg = returnCode: 0 = Success
        if (flg == 0)
            display('[NatNet] Initialization Succeeded')
        else
            display('[NatNet] Initialization Failed')
        end
        
        % print out a list of the active tracking Models in Motive
%         GetDataDescriptions(theClient)
        
        % Test - send command/request to Motive
        [byteArray, retCode] = theClient.SendMessageAndWait('FrameRate');
        if(retCode ==0)
            byteArray = uint8(byteArray);
            frameRate = typecast(byteArray,'single');
        end
        
        % get the mocap data
        if(usePollingTimer)
            % approach 2 : poll using a Matlab timer callback ( better for UI based apps )
            framePerSecond = 200;   % timer frequency
            TimerData = timer('TimerFcn', {@TimerCallback,theClient},'Period',1/framePerSecond,'ExecutionMode','fixedRate','BusyMode','drop');
            start(TimerData);
            % wait until figure is closed
            uiwait(hFigure);
        else
            if(usePollingLoop)
                % approach 1 : get data by polling - just grab 5 secs worth of data in a tight loop
                for idx = 1 : 1000   
                   % Note: sleep() accepts [mSecs] duration, but does not process any events.
                   % pause() processes events, but resolution on windows can be at worst 15 msecs
                   java.lang.Thread.sleep(5);  

                    % Poll for latest frame instead of using event callback
                    data = theClient.GetLastFrameOfData();
                    frameTime = data.fLatency;
                    frameID = data.iFrame;
                    if(frameTime ~= lastFrameTime)
                        fprintf('FrameTime: %0.3f\tFrameID: %5d\n',frameTime, frameID);
                        lastFrameTime = frameTime;
                        lastFrameID = frameID;
                    else
                        display('Duplicate frame');
                    end
                 end
            else
                % approach 3 : get data by event handler (no polling)
                % Add NatNet FrameReady event handler
                ls = addlistener(theClient,'OnFrameReady2',@(src,event)FrameReadyCallback(src,event,hObject, hFigure));
                display('[NatNet] FrameReady Listener added.');
                % wait until figure is closed
                uiwait(hFigure);
            end
        end

    catch err
        display(err);
    end

    % cleanup
    if(usePollingTimer)
        stop(TimerData);
        delete(TimerData);
    end
    theClient.Uninitialize();
    if(useFrameReadyEvent)
        if(~isempty(ls))
            delete(ls);
        end
    end
    clear functions;

    display('NatNet Sample End')
    
end
function keydown(src,event, hObject)
    
    if (event.Key =='f' | event.Key =='F')
        handles = guidata(hObject);
        handles.isfdown = true;
        guidata(handles.figure1, handles);
    end
    
end
% Test : process data in a Matlab Timer callback
function TimerCallback(obj, event, theClient)

    frameOfData = theClient.GetLastFrameOfData();
    UpdateUI( frameOfData );
    
end

% Test : Process data in a NatNet FrameReady Event listener callback
function FrameReadyCallback(src, event, hObject, handles)
    
    frameOfData = event.data;
    UpdateUI( frameOfData, hObject, handles );
    
end

% Update a Matlab Plot with values from a single frame of mocap data
function UpdateUI( frameOfData, hObject, hFigure )

    persistent lastFrameTime;
    persistent lastFrameID;
    persistent hX;
    persistent hY;
    persistent hZ;
    persistent arrayIndex;
    persistent frameVals;
    persistent xVals;
    persistent yVals;
    persistent zVals;
    persistent bufferModulo;

    global frameRate;
    
    % first time - generate an array and a plot
    if isempty(hX)
        % initialize static
    end

    % calculate the frame increment based on mocap frame's timestamp
    % in general this should be monotonically increasing according
    % To the mocap framerate, however frames are not guaranteed delivery
    % so to be accurate we test and report frame drop or duplication
    newFrame = true;
    droppedFrames = false;
    frameTime = frameOfData.fLatency;
    frameID = frameOfData.iFrame;
    calcFrameInc = round( (frameTime - lastFrameTime) * frameRate );
    % clamp it to a circular buffer of 255 frames
    arrayIndex = mod(arrayIndex + calcFrameInc, bufferModulo);
    if(arrayIndex==0)
        arrayIndex = 1;
    end
    if(calcFrameInc > 1)
        % debug
        % fprintf('\nDropped Frame(s) : %d\n\tLastTime : %.3f\n\tThisTime : %.3f\n', calcFrameInc-1, lastFrameTime, frameTime);
        droppedFrames = true;
    elseif(calcFrameInc == 0)
        % debug
        % display('Duplicate Frame')      
        newFrame = false;
    end
    
    % debug
    % fprintf('FrameTime: %0.3f\tFrameID: %d\n',frameTime, frameID);
    
    try
        if(newFrame)
            if(frameOfData.RigidBodies.Length() > 0)
                
                rigidBody2 = frameOfData.RigidBodies(2);
                m = zeros(4,3);
                for i = 1:4
                   m(i, :) = [rigidBody2.Markers(i).x, rigidBody2.Markers(i).y, rigidBody2.Markers(i).z];
                end
                
                handles = guidata(hObject);

%                 Estimate marker locations
                pt2_est = getLocation(handles.pt2_distances, m);
                pt4_est = getLocation(handles.pt4_distances, m);
                pt1_est = getLocation(handles.pt1_distances, m);
                
                set(handles.pt2_lbl, 'String', sprintf('[%0.3f,%0.3f,%0.3f]', pt2_est(1), pt2_est(2), pt2_est(3)));
                set(handles.pt4_lbl, 'String', sprintf('[%0.3f,%0.3f,%0.3f]', pt4_est(1), pt4_est(2), pt4_est(3)));
                set(handles.pt1_lbl, 'String', sprintf('[%0.3f,%0.3f,%0.3f]', pt1_est(1), pt1_est(2), pt1_est(3)));
                guidata(handles.figure1, handles);
                
%                 local reference frame
                p2loc = handles.w.p2.translation;
                p4loc = handles.w.p4.translation;
                p1loc = handles.w.p1.translation;
                
                p3loc = handles.w.p3.translation;
                
                local.midpoint21 = mean([p2loc; p1loc]);
                
                local.ihat = (p1loc - p2loc)/ norm(p1loc - p2loc);
                local.otherVecInPlane = (p4loc - local.midpoint21)/ norm(p4loc - local.midpoint21);
                local.khat = cross(local.ihat, local.otherVecInPlane)/norm(cross(local.ihat, local.otherVecInPlane));
                local.jhat = cross(local.khat, local.ihat);
%                 
                d = p2loc';
                
                icomp = d'*local.ihat';
                jcomp = d'*local.jhat';
                kcomp = d'*local.khat';
                
                %global reference frame
                glob.midpoint21 = mean([pt2_est'; pt1_est'])';
                glob.ihat = (pt1_est - pt2_est)/norm(pt1_est - pt2_est);
                glob.otherVecInPlane = (pt4_est - glob.midpoint21 )/norm(pt4_est - glob.midpoint21);
                glob.khat = cross(glob.ihat, glob.otherVecInPlane)/norm(cross(glob.ihat, glob.otherVecInPlane));
                glob.jhat = cross(glob.khat, glob.ihat);
                
                glob.origin = pt2_est - (icomp*glob.ihat + jcomp*glob.jhat + kcomp*glob.khat);
               
                
                rigidBody1 = frameOfData.RigidBodies(1);
                matmarker = zeros(4,3);
                for i = 1:4
                   matmarker(i, :) = [rigidBody1.Markers(i).x, rigidBody1.Markers(i).y, rigidBody1.Markers(i).z];
                end
                pt = identifyMarkers(matmarker);
                if pt ~= -1
                    dp = [matmarker(pt,1), matmarker(pt,2), matmarker(pt,3)];
                else
                    dp = ones(1,3);
                end
                
                
                at = dp - glob.origin';
                L = [glob.ihat, -glob.khat, glob.jhat];
                dpl = at * L;
                
                localpts = [p2loc; p4loc; p1loc]';
                globalpts = [pt2_est, pt4_est, pt1_est];
                
                [R, L] = computeRandL(localpts, globalpts);
                
                dpSVD = R * dp' + L;
                
%                 handles.w.TargetMarker.translation = dpl;
                handles.w.TargetMarker.translation = dpSVD';
                fprintf('SVD: %0.2f %0.2f %0.2f \t CLP: %0.2f %0.2f %0.2f \n', dpSVD(1),dpSVD(2),dpSVD(3), dpl(1), dpl(2), dpl(3));
                set(handles.TargetDistance, 'String', num2str(norm(p2loc - dpl)));
                
            end
        end
    catch err
        display(err);
    end
    
    lastFrameTime = frameTime;
    lastFrameID = frameID;

end