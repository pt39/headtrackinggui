function x = isDistWithin(dhat, d, e)
    x=false;
    if(dhat <= d+e && dhat >= d-e)
        x = true;
    end
end