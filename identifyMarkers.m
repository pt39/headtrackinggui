%Identifies which markers correspond with which 
function [pt] = identifyMarkers(markers)
%markers - matrix of markers (markers x coordinates)
 d14 = 0.1183;
 d24 = 0.1279;
  
  e = .002;
  
  pt = -1;
  for i = 1:size(markers,1)
      cm= markers(i,:); % current marker
      
      otherind = 1:size(markers, 1);
      otherind(i) = [];
      
      d1 = norm(cm - markers(otherind(1), :));
      d2 = norm(cm - markers(otherind(2), :));
      d3 = norm(cm - markers(otherind(3), :));
      
      if( (isDistWithin(d1, d14,e) && isDistWithin(d2, d24,e)) || (isDistWithin(d2, d14,e) && isDistWithin(d1, d24,e)) )
          pt = i;
          return;
      elseif( (isDistWithin(d2, d14,e) && isDistWithin(d3, d24,e)) || (isDistWithin(d3, d14,e) && isDistWithin(d2, d24,e)) )
          pt = i;
          return;
      elseif( (isDistWithin(d1, d14,e) && isDistWithin(d3, d24,e)) || (isDistWithin(d3, d14,e) && isDistWithin(d1, d24,e)) )
          pt = i;
          return;
      end
      
  end
end